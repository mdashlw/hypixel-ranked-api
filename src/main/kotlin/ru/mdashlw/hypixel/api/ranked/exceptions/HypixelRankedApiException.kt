package ru.mdashlw.hypixel.api.ranked.exceptions

class HypixelRankedApiException(message: String) : RuntimeException(message)
